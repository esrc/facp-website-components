#!/bin/bash
# Do a basic check of the theme and Javascript files to confirm that they are
# using the correct web service URLs for production use. If this is the case,
# then push the production branch into the main project repository.

# !!!!!
# TODO: should really be using a regex to look for data-source="..." and then
# make sure that the data source value corresponds to something that we expect

# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)

# Absolute path this script is in. /home/user/bin
SCRIPTPATH=`dirname $SCRIPT`

# absolute path to the project folder
PROJECT=`dirname $SCRIPTPATH`

# error counter
ERRORS=0

# make sure that there are no URLs pointing to localhost
COUNT_LOCALHOST=`grep "localhost" $PROJECT/theme/*.php  | wc -l`
if [[ $COUNT_LOCALHOST -gt 0 ]];
then
    ERRORS=`expr $ERRORS + $COUNT_LOCALHOST`
    echo "Error: found $COUNT_LOCALHOST instances of 'localhost' in the theme"
fi

# make sure that there are no URLs pointing to data.esrc.info
COUNT_TEST_DATA=`grep "data.esrc.info" $PROJECT/theme/*.php  | wc -l`
if [[ $COUNT_TEST_DATA -gt 0 ]];
then
    ERRORS=`expr $ERRORS + $COUNT_TEST_DATA`
    echo "Error: found $COUNT_TEST_DATA instances of 'data.esrc.info' in the theme"
fi

# make sure that there are no URLs pointing to localhost
COUNT_TEST_FACP=`grep "facp.esrc.info" $PROJECT/theme/*.php  | wc -l`
if [[ $COUNT_TEST_FACP -gt 0 ]];
then
    ERRORS=`expr $ERRORS + $COUNT_TEST_FACP`
    echo "Error: found $COUNT_TEST_FACP instances of 'facp.esrc.info' in the theme"
fi

# make sure that there are no URLs pointing to mailer.esrc.info
COUNT_TEST_MAILER=`grep "mailer.esrc.info" $PROJECT/theme/*.php  | wc -l`
if [[ $COUNT_TEST_MAILER -gt 0 ]];
then
    ERRORS=`expr $ERRORS + $COUNT_TEST_MAILER`
    echo "Error: found $COUNT_TEST_MAILER instances of 'mailer.esrc.info' in the theme"
fi

# if the configuration appears to be correct, then update the production
# branch of the primary repository
if [[ $ERRORS -eq 0 ]];
then
    echo "No configuration errors found. Will push code to production."
    git push origin production
else
    echo "There are $ERRORS incorrectly configured entries. Push to production halted."
fi
