#!/bin/bash
# Do a basic check of the theme and Javascript files to determine if they
# are using non-production web service URLs. If any non-production URLs are
# found then warn the user before pushing the branch to testing.

# !!!!!
# TODO: should really be using a regex to look for data-source="..." and then
# make sure that the data source value corresponds to something that we expect

# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)

# Absolute path this script is in. /home/user/bin
SCRIPTPATH=`dirname $SCRIPT`

# absolute path to the project folder
PROJECT=`dirname $SCRIPTPATH`

# make sure that there are no URLs pointing to localhost
COUNT_LOCALHOST=`grep "localhost" $PROJECT/theme/*.php  | wc -l`
if [[ $COUNT_LOCALHOST -gt 0 ]];
then
    echo "Warning: found $COUNT_LOCALHOST instances of 'localhost' in the theme"
fi

# make sure that there are no URLs pointing to data.esrc.info
COUNT_TEST_DATA=`grep "data.esrc.info" $PROJECT/theme/*.php  | wc -l`
if [[ $COUNT_TEST_DATA -gt 0 ]];
then
    echo "Warning: found $COUNT_TEST_DATA instances of 'data.esrc.info' in the theme"
fi

# make sure that there are no URLs pointing to localhost
COUNT_TEST_FACP=`grep "facp.esrc.info" $PROJECT/theme/*.php  | wc -l`
if [[ $COUNT_TEST_FACP -gt 0 ]];
then
    echo "Warning: found $COUNT_TEST_FACP instances of 'facp.esrc.info' in the theme"
fi

# make sure that there are no URLs pointing to mailer.esrc.info
COUNT_TEST_MAILER=`grep "mailer.esrc.info" $PROJECT/theme/*.php  | wc -l`
if [[ $COUNT_TEST_MAILER -gt 0 ]];
then
    echo "Warning: found $COUNT_TEST_MAILER instances of 'mailer.esrc.info' in the theme"
fi

# push the testing branch
git push origin testing
