#!/bin/bash
# fetch the latest copy of eaccpf-ajax and deploy the required components into
# the template

# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)

# Absolute path this script is in. /home/user/bin
SCRIPTPATH=`dirname $SCRIPT`

# absolute path to the project folder
PROJECT=`dirname $SCRIPTPATH`

TEMP="$PROJECT/temp"
REPO="$TEMP/eaccpf-ajax"
FACP="$PROJECT/search"

# -----------------------------------------------------------------------------
# get the latest copy of eaccpf-ajax

if [ ! -d "$TEMP" ]; then
  mkdir $TEMP
fi

if [ ! -d "$REPO" ]; then
  cd $TEMP
  git clone git@bitbucket.org:esrc/eaccpf-ajax.git
fi

echo "Updating local eacccpf-ajax repo"
cd $REPO
git pull origin master
git pull origin production
git pull origin testing

# -----------------------------------------------------------------------------
# deploy application components into the FACP folders

echo "Clearing existing eaccpf-ajax components in FACP folder"
rm -f $FACP/controllers/*
rm -f $FACP/directives/*
rm -f $FACP/filters/*
rm -f $FACP/services/*

rm -fr $FACP/jquery-ui/*
mkdir $FACP/jquery-ui/images/

echo "Copying latest eaccpf-ajax components to FACP folder"
cp $REPO/app/css/jquery-ui-bootstrap/jquery-ui-1.10.0.custom.css $FACP/jquery-ui/jquery-ui.min.css

cp $REPO/app/js/jquery/* $FACP/jquery-ui/
cp $REPO/app/js/jquery-ui/* $FACP/jquery-ui/

cp $REPO/app/js/solr-ajax/controllers/* $FACP/controllers/
cp $REPO/app/js/solr-ajax/directives/* $FACP/directives/
cp $REPO/app/js/solr-ajax/filters/* $FACP/filters/
cp $REPO/app/js/solr-ajax/services/* $FACP/services/

cp $REPO/app/lib/angular/* $FACP/angular/

echo "Copying icons to FACP folder"
cp $REPO/app/css/jquery-ui-bootstrap/images/* $FACP/jquery-ui/images/
cp $REPO/app/img/icon/* $FACP/img/
cp $REPO/app/img/map/* $FACP/img/

# -----------------------------------------------------------------------------
# modify the icon paths in the map controller

echo "Updating MapController icon paths"
sed -i "s|img/icon/house.png|/search/img/house.png|g" $FACP/controllers/MapController.js
sed -i "s|img/icon/corporatebody.png|/search/img/corporatebody.png|g" $FACP/controllers/MapController.js
sed -i "s|img/icon/person.png|/search/img/person.png|g" $FACP/controllers/MapController.js

sed -i "s|img/map/cluster1.png|/search/img/cluster1.png|g" $FACP/controllers/MapController.js
sed -i "s|img/map/cluster2.png|/search/img/cluster2.png|g" $FACP/controllers/MapController.js
sed -i "s|img/map/cluster3.png|/search/img/cluster3.png|g" $FACP/controllers/MapController.js
sed -i "s|img/map/cluster4.png|/search/img/cluster4.png|g" $FACP/controllers/MapController.js
sed -i "s|img/map/cluster5.png|/search/img/cluster5.png|g" $FACP/controllers/MapController.js

cd $SCRIPTPATH
