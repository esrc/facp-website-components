#!/bin/bash
# update the data source to use the production end point for search results

# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)

# Absolute path this script is in. /home/user/bin
SCRIPTPATH=`dirname $SCRIPT`

# absolute path to the project folder
PROJECT=`dirname $SCRIPTPATH`

PRODUCTION="data.esrc.unimelb.edu.au/solr/FACP"
TESTING="data.esrc.info/solr/FACP"

sed -i "s|$TESTING|$PRODUCTION|g" $PROJECT/theme/*.php
