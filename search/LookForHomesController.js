/**
 * This file is subject to the terms and conditions defined in the
 * 'LICENSE.txt' file, which is part of this source code package.
 */

'use strict';

/*---------------------------------------------------------------------------*/
/* Controller                                                                */

/**
 * Constructor a Solr query compatible URL from the user provided form input.
 * Redirect the user-agent to a page where search results are displayed.
 * @param $scope Controller scope.
 * @param $log Log
 * @param Utils Utils module
 */
function LookForHomesController($scope, $log, Utils) {

    $scope.name = "";           // name of the home
    $scope.state = "";          // state where the home was located
    $scope.suburb = "";         // name of the suburb in which the home was located
    $scope.date = "";           // year in which the home existed
    $scope.organisation = "";   // name of the organization that ran the home

    /**
     * Handle submit click event. Construct a valid Solr query URL from the
     * user input data, then execute a GET call with that URL.
     */
    $scope.handleSubmit = function() {
        var query_root = "./search-results/#/q=";
        var query = "";
        // user query
        if (Utils.trim($scope.name) != "") {
            query += Utils.trim($scope.name) + " ";
        }
        // suburb
        if (Utils.trim($scope.suburb) != "") {
            query += Utils.trim($scope.suburb) + " ";
        }
        // organisation
        if (Utils.trim($scope.organisation) != "") {
            query += Utils.trim($scope.organisation);
        }
        // if the query is empty then set it to the default
        if (Utils.trim(query) == "") {
            query = query_root + "*:*~";
        } else {
            query = query_root + query;
        }
        // exclude digital objects
        query += " -type:\"Digital Object\"";
        // filter by localtype and function
        query += "&fq=localtype:(Organisation)";
        query += "&fq=function:(Home)";
        // filter by date
        if ($scope.date != "") {
            query += "&fq=fromDate:[* TO " + $scope.date['id'] + "-12-31T23:59:59Z]";
            query += "&fq=toDate:[" + $scope.date['id'] + "-01-01T00:00:00Z TO *]";
        }
        // filter by state
        if ($scope.state != "") {
            query += "&fq=region:(" + $scope.state['id'] + ")";
        }
        // options
        query += "&fl=*&start=0&rows=10&wt=json";
        // log the query
        $log.info("QUERY " + query);
        // update the window location, prevent the default form submit behavior
        location.assign(query);
        return false;
    };

    // values for state select field
    $scope.states = [
        {id:'ACT', name:'Australian Capital Territory'},
        {id:'NSW', name:'New South Wales'},
        {id:'NT',  name:'Northern Territory'},
        {id:'QLD', name:'Queensland'},
        {id:'SA',  name:'South Australia'},
        {id:'TAS', name:'Tasmania'},
        {id:'VIC', name:'Victoria'},
        {id:'WA',  name:'Western Australia'}
    ];

    // values for date select field
    $scope.years = [];
    var currentYear = new Date().getFullYear();
    var firstYear = 1830;
    for (var year=currentYear;year>=firstYear;year--) {
        var option = {id: year, name: year};
        $scope.years.push(option);
    }

}

// inject dependencies
LookForHomesController.$inject = ['$scope','$log','Utils'];
