/**
 * This file is subject to the terms and conditions defined in the
 * 'LICENSE.txt' file, which is part of this source code package.
 */
'use strict';

/*---------------------------------------------------------------------------*/
/* SearchBoxController                                                       */

/**
 * Provides basic search box functionality.
 * @param $scope Controller scope
 * @param $attrs
 * @param $location
 * @param $route
 * @param $routeParams
 * @param $window
 * @param SolrSearchService
 * @param Utils Utility
 * @see http://jsfiddle.net/DNjSM/17/
 */
function SimpleSearchBoxController($scope, $attrs, $location, $routeParams, $window, SolrSearchService, Utils) {

    // find near matches to the user query
    $scope.nearMatch = false;

    // query name
    $scope.queryName = SolrSearchService.defaultQueryName;

    // when the user submits the query, redirect to the specified URL, with the
    // query appended, to render the results. If no redirect is specified,
    // update the location hash of the current page.
    $scope.redirect = undefined;

    // If true, when a user enters a new query string, the target query will be
    // replaced with a new query and the user query property will be set, If
    // false, only the user query and start properties will be changed and the
    // query results will be reloaded.
    $scope.resetOnChange = false;

    // url to solr core
    $scope.source = undefined;

    // the query string provided by the user
    $scope.userQuery = "";

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Handle the route change event.
     */
    $scope.handleRouteChangeSuccess = function() {
        if ($routeParams.query != "") {
            var query = SolrSearchService.getQueryFromHash($routeParams.query, $scope.source);
            $scope.userQuery = query.getUserQuery();
        } else {
            $scope.userQuery = $routeParams.query;
        }
    };

    /**
     * Handle submit click event. Construct a valid Solr query URL from the
     * user input data, then execute a GET call with that URL.
     */
    $scope.handleSubmit = function() {
        // clean up the user query
        var trimmed = Utils.trim($scope.userQuery);
        if (trimmed === '') {
            $scope.userQuery = "*:*";
        }
        // build the query string
        var query = SolrSearchService.createQuery($scope.source);
        query.setNearMatch($scope.nearMatch);
        query.setUserQuery($scope.userQuery);
        // update the window location
        var hash = query.getHash();
        if ($scope.redirect) {
            $window.location.href = $scope.redirect + '#' + hash;
        } else {
            $location.path(hash);
        }
    };

    /**
     * Initialize the controller. Apply configured attribute values.
     * Handle location change events, update the query value.
     */
    $scope.init = function() {
        Utils.applyAttributes($attrs, $scope);
        $scope.$on("$routeChangeSuccess", function() {
            $scope.handleRouteChangeSuccess()
        });
    };

    // initialize the controller
    $scope.init();

}

// inject controller dependencies
SimpleSearchBoxController.$inject = ['$scope','$attrs','$location','$routeParams','$window','SolrSearchService','Utils'];
