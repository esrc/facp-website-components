/**
 * This file is subject to the terms and conditions defined in the
 * 'LICENSE.txt' file, which is part of this source code package.
 */

'use strict';

/*---------------------------------------------------------------------------*/
/* Controller                                                                */

/**
 * Handles construction of a Solr query on the browse page.
 * @param $scope Controller scope.
 * @param $attrs Element attributes
 * @param Utils Utils module
 * @constructor
 */
function BrowseController($scope, $attrs, Utils) {

    $scope.userQuery = "";      // user query

    /**
     * Handle submit click event. Construct a valid Solr query URL from the
     * user input data, then execute a GET call with that URL.
     */
    $scope.handleSubmit = function() {
        var query = "./search-results/#/q=";
        // user query
        if (Utils.trim($scope.userQuery) != "") {
            query += Utils.trim($scope.userQuery) + "~";
        } else {
            query += "*:*~";
        }
        // make the query near matching
        query += "~";
        // options
        query += "&fl=*&start=0&rows=10&wt=json";
        // log the query
        if (window.console) {
            console.log("QUERY " + query);
        }
        // update the window location, prevent the default form submit behavior
        location.assign(query);
        return false;
    };

    /**
     * Initialize the controller.
     */
    $scope.init = function() {
        // apply configured attributes
        for (var key in $attrs) {
            if ($scope.hasOwnProperty(key)) {
                $scope[key] = $attrs[key];
            }
        }
    };

    // initialize the controller
    $scope.init();

}

// inject dependencies
BrowseController.$inject = ['$scope', '$attrs', 'Utils'];
