function init() {
    var h2s = $(".entry-extras h2");
    if (h2s.length > 0) {
        var openAll = $('<div class="entry-extras entry-open-all"><a href="#">Read more</a></div>');
        openAll.click(function() {
            if ($(this).hasClass("entry-open-all-open")) {
                $("a", this).text("Read more");
                h2s.each(function() {
                    if (!$(this).parents(".entry-extras").hasClass("closed")) {
                        $(this).click();
                    }
                });
        } else {
            $("a", this).text("Less");
            h2s.each(function() {
                if ($(this).parents(".entry-extras").hasClass("closed")) {
                    $(this).click();
                }
            })
        }
        $(this).toggleClass("entry-open-all-open");
        }).insertBefore($(".entry-extras").eq(0))
    }
    h2s.each(function() {
        $(this).parents(".entry-extras").addClass("closed");
        $(this).wrapInner('<a href="#"></a>');
    }).click(function() {
        $(this).parents(".entry-extras").toggleClass("closed");
    });
    $(".entry-extras a[href=#]").click(function(event) {
        event.preventDefault();
    });
}

$(init);