/**
 * This file is subject to the terms and conditions defined in the
 * 'LICENSE.txt' file, which is part of this source code package.
 */

test("get_query_property", function() {
    var cases = [
        {
            "url": "http://www.example.com/",
            "expected_t": "",
            "expected_u": ""
        },
        {
            "url": "http://www.example.com/1",
            "expected_t": "",
            "expected_u": ""
        },
        {
            "url": "http://www.example.com/?u=*.*",
            "expected_t": "",
            "expected_u": "*.*"
        },
        {
            "url": "http://www.example.com/?t=This%20is%20a%20test!%3F",
            "expected_t": "This is a test!?",
            "expected_u": ""
        },
        {
            "url": "http://www.example.com/?u=*.*%26abc%3Dxyz%26xyz%3Dabc",
            "expected_t": "",
            "expected_u": "*.*&abc=xyz&xyz=abc"
        },
        {
            "url": "http://www.example.com/?t=What%3F%3F%3F&u=http%3A%2F%2Fwww.example.com%2F%3Fq%3D*.*",
            "expected_t": "What???",
            "expected_u": "http://www.example.com/?q=*.*"
        }
    ];
    for (var i=0;i<cases.length;i++) {
        var c = cases[i];
        var url = c['url'];
        var expected_title = c['expected_t'];
        var expected_url = c['expected_u'];
        var result_url = get_query_property(url, 'u');
        var result_title = get_query_property(url, 't');
        equal(expected_title, result_title, "Result title equals expected title");
        equal(expected_url, result_url, "Result URL equals expected URL");
    }

});

test( "get_referrer_title", function() {
    var cases = [
        {
            "url":"http://localhost:8080/contact/ask-us/?",
            "expected": ""
        },
        {
            "url":"http://localhost:8080/contact/ask-us/?page_title=This is a test",
            "expected": "This is a test"
        },
        {
            "url":"http://localhost:8080/contact/ask-us/?referrer=http://localhost:8080/referring/page#fragment&page_title=This is a test",
            "expected": "This is a test"
        },
        {
            "url":"http://localhost:8080/contact/ask-us/?referrer=http://localhost:8080/referring/page#fragment&",
            "expected": ""
        },
        {
            "url":"http://www.findandconnect.gov.au/contact/ask-us/?referrer=http://www.findandconnect.gov.au/look-for-homes/search-results/#/q=*:*%20-type:%22Digital%20Object%22&fq=localtype:(Organisation)&fq=function:(Home)&fl=*&start=0&rows=10&wt=json&page_title=Look for Homes – search results | Find & Connect",
            "expected":"Look for Homes – search results | Find & Connect"
        }
    ];
    for (var i=0;i<cases.length;i++) {
        var c = cases[i];
        url = c['url'];
        expected = c['expected'];
        result = get_referrer_title(url);
        equal(result, expected, "Expected and result title are equal");
    }
});

test( "get_referrer_url", function() {
    var cases = [
        {
            "url":"http://localhost:8080/contact/ask-us/?",
            "expected": ""
        },
        {
            "url":"http://localhost:8080/contact/ask-us/?page_title=This is a test",
            "expected": ""
        },
        {
            "url":"http://localhost:8080/contact/ask-us/?referrer=http://localhost:8080/referring/page#fragment&page_title=This is a test",
            "expected": "http://localhost:8080/referring/page#fragment"
        },
        {
            "url":"http://localhost:8080/contact/ask-us/?referrer=http://localhost:8080/referring/page#fragment",
            "expected": "http://localhost:8080/referring/page#fragment"
        },
        {
            "url":"http://localhost:8080/contact/ask-us/?referrer=http://localhost:8080/referring/page",
            "expected": "http://localhost:8080/referring/page"
        },
        {
            "url":"http://www.findandconnect.gov.au/contact/ask-us/?referrer=http://www.findandconnect.gov.au/look-for-homes/search-results/#/q=*:*%20-type:%22Digital%20Object%22&fq=localtype:(Organisation)&fq=function:(Home)&fl=*&start=0&rows=10&wt=json&page_title=Look for Homes – search results | Find & Connect",
            "expected":"http://www.findandconnect.gov.au/look-for-homes/search-results/#/q=*:*%20-type:%22Digital%20Object%22&fq=localtype:(Organisation)&fq=function:(Home)&fl=*&start=0&rows=10&wt=json"
        }
    ];
    for (var i=0;i<cases.length;i++) {
        var c = cases[i];
        url = c['url'];
        expected = c['expected'];
        result = get_referrer_url(url);
        equal(result, expected, "Expected and result URL are equal");
    }
});

