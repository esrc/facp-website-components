/**
 * On other pages in the site, we provide a "Send us a question or comment"
 * button that redirects to the contact form page. The redirect URL includes a
 * query that is appended on, with parameters of referring page URL and
 * referring page title. The functions below extract those values from the URL
 * and inject them into hidden input fields in the contact form so that they
 * can be forwarded on to the mailer application.
 */
'use strict'

/**
 * Get the redirect value from the query portion of the current
 * browser location.
 */
function get_query_property(location, parameter) {
    var i = location.indexOf('?');
    if (i != -1) {
        var query = location.substring(i+1);
        var parts = query.split('&');
        for (var j=0;j<parts.length;j++) {
            var part = parts[j];
            // if the query part starts with the parameter name
            if (part.indexOf(parameter) == 0) {
                var val = part.substring(parameter.length+1);
                return decodeURIComponent(val);
            }
        }
    }
    return '';
}

/**
 * Redirect to the contact form for the specified team. Valid team values are
 * 'support-team' or 'web-resource'. Defaults to support team.
 * @param team
 */
function send(team) {
    var host = location.protocol +  "//" + window.location.hostname;
    var endpoint = 'support-service';
    if (team != undefined && team == 'web-resource') {
        endpoint = "ask-us";
    }
    var path = "/contact/" + endpoint + "/?u=" + encodeURIComponent(window.location) + "&t=" + encodeURIComponent(document.title);
    var url = host + path;
    window.location = url;
}

/**
 * Update the hidden form parameters with arguments from the location query.
 */
function update_hidden_parameters() {
    // the prior implementation of this script looked for values
    // 'referrer' and 'page_title' in the URL. we'll leave this
    // here for short term backward compatibility.
    var title = get_query_property(window.location.href, 't');
    var url = get_query_property(window.location.href, 'u');
    // the new iteration looks for u and t respectively
    var referring_page_title = get_query_property(window.location.href, 'page_title');
    var referring_page_url = get_query_property(window.location.href, 'referrer');
    // if the old values are present then use them
    // otherwise use the new values
    if (title != "" || url != "") {
        document.getElementById('referring_page_title').value = title;
        document.getElementById('referring_page_url').value = url;
    }
    if (referring_page_title != "" || referring_page_url != "") {
        document.getElementById('referring_page_title').value = referring_page_title;
        document.getElementById('referring_page_url').value = referring_page_url;
    }
}
