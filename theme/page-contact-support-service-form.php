<?php
/**
 * Template Name: Contact Support Service, email contact form
 * @package FindAndConnect
 */
get_header(); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">

            <h1>Send message to Find &amp; Connect support service</h1>
            <p>
                The Find &amp; Connect support service is for people who lived in orphanages and children's
                institutions. Read our <a href="/about/terms-and-conditions/">privacy policy</a>.
            </p>

            <form class="form-margin" action="http://mailer.esrc.unimelb.edu.au/send/FACP" method="post">

                <input type="hidden" id="team" name="team" value="support_service" readonly>
                <input type="hidden" id="referring_page_title" name="referring_page_title" value="" readonly />
                <input type="hidden" id="referring_page_url" name="referring_page_url" value="" readonly />
                <input type="hidden" id="formtype" name="formtype" value="askus" />

                <div id="js_referrer"></div>

				<div class="form-container">
					<label class="label-wide" for="feedback"><strong>Your message</strong></label>
					<textarea class="textarea-wide" name="feedback" rows="5" cols="50" required></textarea>
					<div class="indent font14" style="clear:both;">If you know the name of the Home you're looking for, please include it in your message.</div>
				</div>
				<div class="form-container">
					<label class="label-wide" for="name"><strong>Your name</strong></label>
					<input class="text-wide" name="name" id="name" type="text" required />
					<div class="indent font14">We need to know this so that we know how to address you</div>
				</div>
				<div class="form-container">
					<label class="label-wide" for="email"><strong>Your email</strong></label>
					<input class="text-wide" name="email" id="email" type="email" required />
					<div class="indent font14">We need to know this so that we can respond to your feedback</div>
				</div>
				<div class="form-container">
					<label class="label-wide" for="state"><strong>Where do you live?</strong></label>
					<select name="state" id="state" required>
					    <option value=""></option>
					    <option value="Australian Capital Territory">Australian Capital Territory</option>
					    <option value="New South Wales">New South Wales</option>
					    <option value="Northern Territory">Northern Territory</option>
					    <option value="Queensland">Queensland</option>
					    <option value="South Australia">South Australia</option>
					    <option value="Tasmania">Tasmania</option>
					    <option value="Victoria">Victoria</option>
					    <option value="Western Australia">Western Australia</option>
					    <option value="Outside Australia">Outside Australia</option>
					</select>
					<div class="indent font14">We need to know this so that we can direct your enquiry to the appropriate Find &amp; Connect service</div>
				</div>
				<div class="indent">
					<input class="btn" name="submit" id="submit" type="submit" value="&nbsp;&nbsp;Send&nbsp;&nbsp;">
				</div>

				<!-- extract arguments from the query and assign them to the
				     hidden form fields
				-->
                <script src="https://www.esrc.unimelb.edu.au/lib/contact.js"></script>
			</form>

        </div>
    </div>
</div>

<?php get_footer(); ?>

