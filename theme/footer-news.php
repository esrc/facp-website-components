<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

<div class="row footer-outer">
	<div class="row container footer" role="contentinfo">
		<div class="section-home">
		<ul id="utility-links">
			<li class="font14"><a onclick="send('web-resource');return false;" href="/contact/ask-us/?">Send feedback about website</a></li>
		
	<li class="font14"><a href="http://eepurl.com/7Osvj">Sign up to our newsletter</a></li>
		</ul>	
		<ul id="page-information">
			<li class="font14">Cite this: <?php the_permalink(); ?></li>
		</ul>
		</div>
		<div class="aside-home">
	<p class="cc-logo">
			<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/au/">
				<img src="/assets/news/images/by-nc-sa.png" alt="Creative commons"></a> 
			</p>
			<p class="font13 cc-text">
			Except where otherwise noted, content on this site is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/au/">Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a> 
		  </p>
			</div>
	</div>
</div>

<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-26872007-1']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>

</body>
</html>
