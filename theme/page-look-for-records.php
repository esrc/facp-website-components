<?php
/**
 * Template Name: Look for Records page template
 * @package FindAndConnect
 */
get_header('nocache'); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">

            <div class="notice">
                Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
            </div>
            <h1>
				Look for Records
            </h1>
            <p>
				This page is no longer available and has been redirected to <a href="/information-about-records/">Information about Records</a> (November 2015).
            </p>
            <p>
				To search for information about records related to a particular children’s Home or institution, go to <a href="/look-for-homes/">Look for Homes</a>. 
            </p>

            
        </div>
    </div>
</div>

<?php get_footer(); ?>
