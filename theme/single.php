<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header('news'); ?>

<div class="row main-outer">
	<div class="row container main">
<div class="section">
			<div class="navigation navigation-home">
				<div class="alignleft">
				 <a href="/news" rel="next">More news</a></div>
			</div>		
			
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="entry">
			<h2><?php the_title(); ?></h2>
			<p class="metadetails">
			 Posted on <?php the_time('j F Y') ?> by <?php the_author_posts_link(); ?><br />
			 <?php the_tags(); ?>			
			</p>
			<?php the_content(); ?>
			<p><?php edit_post_link('Edit','','',''); ?></p>
			</div>
			
	        <?php endwhile; else: ?>
		      <p>Sorry, no posts matched your criteria.</p>
	        <?php endif; ?>			
							
		<div class="navigation">
			<div class="alignleft"><?php next_post_link('%link', 'Next post') ?></div>
			<div class="alignright"><?php previous_post_link('%link', 'Previous post') ?></div>
		</div>
	
		</div>	
<?php get_sidebar('news'); ?>
</div>
</div>	
<?php get_footer('news-single'); ?>
