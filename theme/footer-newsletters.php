<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

<div class="intro">
		<p class="font14 noMargin">
			This newsletter is published by the Find &amp; Connect Web Resource Project Team. 
For enquiries please email the editor: <a href="mailto:find-connect@unimelb.edu.au">find-connect@unimelb.edu.au</a> 
<br>
<a href="http://eepurl.com/7Osvj">Sign up to our newsletter.</a>
                </p>
</div>

	<div class="dark">
		<p class="font14 noMargin">
			Funded by the Australian Government.
		</p>
<p class="font14 noMargin">
			Except where otherwise noted, content on this site is licensed under a<br />
 <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/au/">Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a> 
		</p>
	</div>
</div>
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-26872007-1']);
    _gaq.push(['_trackPageview']);
    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
</body>
</html>
