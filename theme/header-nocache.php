<?php
/**
 * Page header.
 * @package FindAndConnect
 */
?><!DOCTYPE html>
<?php if (is_page('look-for-homes') || is_page('look-for-photos') || is_page('look-for-records') ||
          is_page('search-results') || is_page('advanced-search') || is_page('map') ||
          is_page('browse') ) { ?>
<html lang="en" xmlns:ng="http://angularjs.org" id="ng-app" ng-app="facp">
<?php } else { ?>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<?php } ?>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Author" lang="en" content="Find and Connect Web Resource Project, The University of Melbourne and Australian Catholic University" />
<meta name="Description" lang="en" content="Find and Connect is a resource for people who as children were in out-of-home 'care' in Australia. It contains information about organisations, people, policies, legislation and events related to the history of child welfare." />
<meta name="Keywords" lang="en" content="Find and Connect Australia, Find and Connect, Forgotten Australians, Former Child Migrants, Stolen Generations, history of child welfare in Australia" />
<meta name="DC.Creator" lang="en" content="Find and Connect Web Resource Project, The University of Melbourne and Australian Catholic University" />
<meta name="DC.Description" lang="en" content="Find and Connect Web Resource - Find and Connect is a resource for people who as children were in out-of-home 'care' in Australia. It contains information about organisations, people, policies, legislation and events related to the history of child welfare." />
<meta name="DC.Format" scheme="IMT" lang="en" content="text/html" />
<meta name="DC.Identifier" scheme="URL" lang="en" content="<?php echo the_permalink() ?>" />
<meta name="DC.Language" scheme="ISO639" lang="en" content="en-gb" />
<meta name="DC.Publisher" lang="en" content="eScholarship Research Centre, The University of Melbourne" />
<meta name="DC.Rights" lang="en" content="Find and Connect Web Resource Project, The University of Melbourne and Australian Catholic University" />
<meta name="DC.Subject" lang="en" content="Find and Connect Australia, Find and Connect, Forgotten Australians, Former Child Migrants, Stolen Generations, history of child welfare in Australia" />
<meta name="DC.Title" lang="en" content="<?php wp_title( '|', true, 'right' ); ?>" >
<meta name="DC.Type" lang="en" content="Document" />
	<meta name="robots" content="index, follow" />
	<meta name="robots" content="noodp" />
	<meta name="robots" content="noydir" />
	<meta http-equiv="pragma" content="no-cache" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-touch-icon-152x152-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-touch-icon-120x120-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/apple-touch-icon-76x76-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png">
	<link rel="icon" href="/favicon.ico" />
	<link rel="stylesheet" href="/assets/css/normalize.css" />
	<link rel="stylesheet" href="/assets/css/base.css" />
	<link rel="stylesheet" href="/assets/css/layout.css" />
	<link rel="stylesheet" href="/assets/css/modules.css" />
	<link rel="stylesheet" href="/assets/css/modules-buttons.css" />
	<link rel="stylesheet" href="/assets/css/modules-forms.css" />
	<link rel="stylesheet" href="/assets/css/modules-nav.css" />
<?php if (is_page('look-for-homes') || is_page('look-for-photos') || is_page('look-for-records') ||
          is_page('search-results') || is_page('advanced-search') || is_page('map') ||
          is_page('browse')) { ?>
    <link rel="stylesheet" type="text/css" href="/search/search.css" />
    <link rel="stylesheet" type="text/css" href="/search/jquery-ui/jquery-ui.min.css" media="screen" />
<?php } ?>
    <script src="/assets/js/responsive-nav2.js"></script>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="/assets/js/respond.min.js"></script>
	<![endif]-->
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-26872007-1']);
	_gaq.push(['_trackPageview']);
	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>
</head>
<body>
	<div class="row header-outer">
		<div class="row container header" role="banner">
			<div class="header-logo">
				<a href="/"><span><img src="/assets/img/header-logo-narrow.png" alt="Find &amp; Connect"></span></a>
			</div>
			<div class="header-details">
				<span class="header-details-small">Find &amp; Connect Support Services</span><br>
				<span class="header-details-large"><strong><a class="tel" href="tel:+611800161109">1800 16 11 09</a></strong></span><br>
				<span class="header-details-small">Freecall: Monday-Friday 9am-5pm</span>
			</div>
		</div>
	</div>

<div class="row nav-outer">
<div class="row container nav" id="nav" role="navigation">
<ul>
<li class="nav-home <?php if (is_front_page()) { echo "current-home"; }?>"><a href="/">Home</a></li>
<li class="nav-about <?php if (is_page('about') || $post->post_parent == '9') { echo "current"; }?>"><a href="/about/"><strong class="nav-large">ABOUT</strong> <span class="nav-small">Find &amp; Connect</span></a></li>
<li class="nav-homes <?php if (is_page('look-for-homes')) { echo "current"; }?>"><a href="/look-for-homes/"><span class="nav-small">Look for</span> <strong class="nav-large">HOMES</strong></a></li>
<li class="nav-photos <?php if (is_page('look-for-photos')) { echo "current"; }?>"><a href="/look-for-photos/"><span class="nav-small">Look for</span> <strong class="nav-large">PHOTOS</strong></a></li>
<li class="nav-records <?php if (is_page('look-for-records')) { echo "current"; }?>"><a href="/information-about-records/"><span class="nav-small">Information about</span> <strong class="nav-large">RECORDS</strong></a></li>
<li class="nav-browse <?php if (is_page('browse')) { echo "current"; }?>"><a href="/browse/"><strong class="nav-large">SEARCH</strong> <span class="nav-small">this Site</span></a></li>
<li class="nav-contact <?php if (is_page('contact') || $post->post_parent == '34') { echo "current"; }?>"><a href="/contact/"><strong class="nav-large">CONTACT</strong> <span class="nav-small">Support Services</span></a></li>
</ul>
</div>
</div>
