<?php
/**
 * Template Name: Contact Support Service, message processing failure
 * @package FindAndConnect
 */
get_header(); ?>

<!-- page-contact-support-services-failure.php -->
<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            <?php endwhile; else: ?>
            <?php endif; ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>