<?php
/**
 * The template for displaying all pages.
 * @package FindAndConnect
 */

get_header(); ?>

<!-- page.php -->
<div class="row nav-outer">
    <div class="row container nav" id="nav" role="navigation">
        <ul>
            <li class="nav-home"><a href="/">Home</a></li>
            <li class="nav-about"><a href="/about/"><strong class="nav-large">ABOUT</strong> <span class="nav-small">Find &amp; Connect</span></a></li>
            <li class="nav-homes"><a href="/look-for-homes/"><span class="nav-small">Look for</span> <strong class="nav-large">HOMES</strong></a></li>
            <li class="nav-photos"><a href="/look-for-photos/"><span class="nav-small">Look for</span> <strong class="nav-large">PHOTOS</strong></a></li>
            <li class="nav-records"><a href="/look-for-records/"><span class="nav-small">Look for</span> <strong class="nav-large">RECORDS</strong></a></li>
            <li class="nav-browse"><a href="/browse/"><strong class="nav-large">BROWSE</strong> <span class="nav-small">Find &amp; Connect</span></a></li>
            <li class="nav-help"><a href="/help/"><strong class="nav-large">HELP</strong> <span class="nav-small">with this site</span></a></li>
            <li class="nav-contact"><a href="/contact/"><strong class="nav-large">CONTACT</strong> <span class="nav-small">support/counselling</span></a></li>
        </ul>
    </div>
</div>

<div class="row main-outer">
    <div class="row container main">
        <div class="aside" id="subnav" role="complementary">
            <?php get_sidebar(); ?>
        </div>
        <div class="section">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                <div class="post">
                    <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    <small><?php the_time('F jS, Y') ?> by <?php the_author_posts_link() ?></small>
                    <div class="entry"><?php the_content(); ?></div>
                    <p class="postmetadata">Posted in <?php the_category(', '); ?></p>
                </div>
            <?php endwhile; else: ?>
                <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>