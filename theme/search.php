<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header('news'); ?>

<div class="row main-outer">
	<div class="row container main">
<div class="section">	
	  <?php if (have_posts()) : ?>
		<h2>Search results</h2>
	<?php while (have_posts()) : the_post(); ?>
	<div class="entry">
		<h3 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
		<p class="metadetails">
			 Posted on <?php the_time('j F Y') ?> by <?php the_author_posts_link(); ?><br />
			 <?php the_tags(); ?>			
			</p>
	<?php the_excerpt() ?>
	</div>
	<?php endwhile; ?>		
	<?php else : ?>
		<h2>There are no posts in the News containing your search terms.  Search the Find &amp; Connect <a href="http://www.findandconnect.gov.au/">main website</a> instead?
</h2>
				
	  <?php endif; ?>
		<div class="navigation">
			<div class="alignleft"><?php next_post_link('%link', 'Next post') ?></div>
			<div class="alignright"><?php previous_post_link('%link', 'Previous post') ?></div>
		</div>
	</div>	
<?php get_sidebar('news'); ?>
</div>
</div>	
<?php get_footer('news'); ?>