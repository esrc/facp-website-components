<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

<div class="aside">
		<?php get_search_form(); ?>
		 <div class="entry">
			<h4>About Find &amp; Connect</h4>
			<p>History and information about Australian orphanages, children's Homes and other institutions.
				</p>			
			<ul class="links-home">
					<li><a href="http://www.findandconnect.gov.au">Find &amp; Connect home</a></li>
				</ul>
		</div>		
		<div class="entry">
			<h4>Tags</h4>
			<ul class="links">
			  <?php list_tags('show_count=1&title_li='); ?>
			</ul> 			
		</div>
		<div class="entry">
		<h4>Categories</h4>
			<ul class="links">
				<?php wp_list_categories('show_count=1&title_li='); ?>
			</ul>
		</div>
		<div class="entry">
			<h4>Archives</h4>
			<ul class="links">
				<?php wp_get_archives('show_post_count=1'); ?>
			</ul>
			</div>
</div>
