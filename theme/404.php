<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * Template Name: Error template
 */

get_header(); ?>

<!-- error.php -->
<div class="row nav-outer">
    <div class="row container nav" id="nav" role="navigation">
        <ul>
            <li class="nav-home"><a href="/">Home</a></li>
            <li class="nav-about"><a href="/about/"><strong class="nav-large">ABOUT</strong> <span class="nav-small">Find &amp; Connect</span></a></li>
            <li class="nav-homes"><a href="/look-for-homes/"><span class="nav-small">Look for</span> <strong class="nav-large">HOMES</strong></a></li>
            <li class="nav-photos"><a href="/look-for-photos/"><span class="nav-small">Look for</span> <strong class="nav-large">PHOTOS</strong></a></li>
            <li class="nav-records"><a href="/look-for-records/"><span class="nav-small">Look for</span> <strong class="nav-large">RECORDS</strong></a></li>
            <li class="nav-browse"><a href="/browse/"><strong class="nav-large">BROWSE</strong> <span class="nav-small">Find &amp; Connect</span></a></li>
            <li class="nav-help"><a href="/help/"><strong class="nav-large">HELP</strong> <span class="nav-small">with this site</span></a></li>
            <li class="nav-contact"><a href="/contact/"><strong class="nav-large">CONTACT</strong> <span class="nav-small">support/counselling</span></a></li>
        </ul>
    </div>
</div>

<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">
            <h4>Something is wrong... we can't find the page you're after!</h4>

            Try the search options above to see if you can locate the page you're looking for.<br/></br/>

            You can also let the web team know using the <a href="/contact/ask-us/">send feedback about website </a> link .<br/><br/>

        </div>
    </div>
</div>

<?php get_footer(); ?>
