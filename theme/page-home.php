<?php
/**
 * Template Name: Home page template
 * @package FindAndConnect
 */

get_header(); ?>

<div class="row feature-outer">
    <div class="row container feature">
        <div class="row section-home feature-text">
            A resource for Forgotten Australians, Former Child Migrants and anyone interested in the
            <strong class="colour1">history of child welfare in Australia</strong>.
<div class="row font16">
            Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
        </div>
        </div>
        <div class="row">
            <ul class="feature-btn">
                <li><a href="/look-for-homes/"><span class="font20">LOOK FOR </span><strong class="font34">Homes</strong></a></li>
                <li><a href="/look-for-photos/"><span class="font20">LOOK FOR </span><strong class="font34">Photos</strong></a></li>
                <li class="noicon"><a href="/information-about-records/"><span class="font20">INFORMATION ABOUT</span><strong class="font34">Records</strong></a></li>
            </ul>
        </div>
        
    </div>
</div>
<div class="row feature2-outer">
    <div class="row container feature2">
        <div class="col-wide">
            <h2 class="feature2-heading font18">BLOG</h2>
            <div class="box">
			    <div class="box-body">
                        <img class="feature-box" src="/assets/img/blog.png" alt="Jigsaw">
<?php 
function cache_time($seconds)
{
	return 100;
}
add_filter( 'wp_feed_cache_transient_lifetime' , 'cache_time' );
$rss = fetch_feed('http://www.findandconnectwrblog.info');
 if (!is_wp_error( $rss ) ) : 
    $maxitems = $rss->get_item_quantity(1); 
    $rss_items = $rss->get_items(0, $maxitems); 
endif;
?>
 <?php
function shorten($string, $length)
{
    $suffix = '&hellip;';
$short_desc = trim(str_replace(array("/r", "/n", "/t"), ' ', strip_tags($string)));
    $desc = trim(substr($short_desc, 0, $length));
    $lastchar = substr($desc, -1, 1);
     if ($lastchar == '.' || $lastchar == '!' || $lastchar == '?') $suffix='';
 $desc .= $suffix;
 return $desc;
}
?>
    <?php 
     if ($maxitems == 0) echo '<li>No items.</li>';
     else 
     foreach ( $rss_items as $item ) : ?>
         <h3 class="font22 mbs">
			<a href='<?php echo esc_url( $item->get_permalink() ); ?>' title='<?php echo esc_html( $item->get_title() ); ?>'> <?php echo esc_html( $item->get_title() ); ?></a>
		 </h3> 
			<p><?php echo shorten($item-> get_description(),'140');?></p>
			</div>
    <?php endforeach; wp_reset_postdata();?>
            </div>
        </div>
        <div class="col-wide col-last">
            <h2 class="feature2-heading font18">FEATURED</h2>
            <div class="box">
                <div class="box-body">
                    <img class="feature-box" src="/assets/img/exhibition.png" alt="Handwriting">
                    <h3 class="font22 mbs">
                        <a href="/featured-stories/what-to-expect-when-accessing-records/">What to expect when accessing records about you</a>
                    </h3>
                    <p>
                        If you spent time in 'care', there may be records about
                        you. This is a guide to your rights, and different
                        types of records and experiences.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>