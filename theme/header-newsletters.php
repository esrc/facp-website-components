<?php
/**
 * Newsletter page header.
 * @package Find and Connect
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="SHORTCUT ICON" href="favicon.ico" />
	<title>Find &amp; Connect Web Resource Newsletter</title>
	<meta charset="utf-8">
    <meta name="DC.Title" lang="en" content="Find and Connect Web Resource Newsletter" />
	<meta name="DC.Creator" lang="en" content="Find and Connect Web Resource Project, The University of Melbourne and Australian Catholic University" />
	<meta name="DC.Subject" lang="en" content="Find and Connect News, Newsletter, Find and Connect Australia, Find and Connect, Forgotten Australians, Former Child Migrants, Stolen Generations, history of child welfare in Australia" />
	<meta name="DC.Description" lang="en" content="Find and Connect Web Resource Newsletter - Find and Connect is a resource for people who as children were in out-of-home 'care' in Australia. It contains information about organisations, people, policies, legislation and events related to the history of child welfare." />
	<meta name="DC.Publisher" lang="en" content="eScholarship Research Centre, The University of Melbourne" />
	<meta name="DC.Type" lang="en" content="Document" />
	<meta name="DC.Format" scheme="IMT" lang="en" content="text/html" />
    <meta name="DC.Identifier" scheme="URL" lang="en" content="<?php echo the_permalink() ?>" />
    <meta name="DC.Language" scheme="ISO639" lang="en" content="en-gb" />
	<meta name="DC.Rights" lang="en" content="Find and Connect Web Resource Project, The University of Melbourne and Australian Catholic University" />
	<meta name="Author" lang="en" content="Find and Connect Web Resource Project, The University of Melbourne and Australian Catholic University" />
	<meta name="Description" lang="en" content="Find and Connect News - Find and Connect is a resource for people who as children were in out-of-home 'care' in Australia. It contains information about organisations, people, policies, legislation and events related to the history of child welfare." />
	<meta name="Keywords" lang="en" content="Find and Connect News, Newsletter, Find and Connect Australia, Find and Connect, Forgotten Australians, Former Child Migrants, Stolen Generations, history of child welfare in Australia" />
	<meta name="robots" content="index, follow" />
	<meta name="robots" content="noodp" />
	<meta name="robots" content="noydir" />
	<link rel="stylesheet" href="/assets/news/newsletter-screen.css" media="screen" />
	<link rel="stylesheet" href="/assets/news/newsletter-print.css" media="print" />
	<script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.1.min.js"></script>
	<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-26872007-1']);
	_gaq.push(['_trackPageview']);
	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>
	<script type="text/javascript">
<!--//--><![CDATA[//><!--
  $(function() {
     if (document.title) {
       var uplink = $("a#feedback-link");
       var pg_title = encodeURIComponent($(this).attr('title'));
       $(uplink).attr("href", function(ind, attr) {
         $(this).attr("href", uplink.attr("href")+  '?t='+  pg_title);
       });
     }
  });
 //--><!]]>
 </script>
</head>
<body>

<!-- header-newsletters.php -->
<div class="container">
	<div class="header">
		<h1 class="noMargin">
			<a href="/news/category/newsletter/"><img src="/assets/news/images/newsletter-logo.png" alt="Find &amp; Connect Web Resource Newsletter" /></a>
		</h1>
		<div class="date font14">
			<?php the_meta(); ?>
			<a class="home-link" href="/news/category/newsletter/">Newsletters Home</a>
		</div>
	</div>
	<div class="dark clearLeft">
		<h2 class="font14 noMargin fontNormal">
			A web resource for Forgotten Australians and Former Child Migrants
		</h2>
	</div>
