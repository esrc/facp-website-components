<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header('newsletters'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php the_content(); ?>
    <p><?php edit_post_link('Edit','','',''); ?></p>

    <?php endwhile; else: ?>
      <p>Sorry, no posts matched your criteria.</p>
    <?php endif; ?>


<?php get_footer('newsletters'); ?>
