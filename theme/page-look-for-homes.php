<?php
/**
 * Template Name: Look for Homes, page template
 * @package FindAndConnect
 */
get_header('nocache'); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">

            <div class="notice">
                Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
            </div>

            <h1>Look for Homes</h1>
			                         
			<h2>Quick Search</h2>
            
            <p>
                NOTE: Searching this website may not work with some versions of the Internet Explorer browser. For more information on the issue please read our <a href="http://www.findandconnectwrblog.info/2015/07/issue-with-search/">blog post</a>.
            </p>
            
            <script type="text/javascript">
                // This function is required on all pages that have a search box. The
                //  submit button MUST be connected to this function. Also, ensure
                //  the id of the input element is "search-form". If your faceted
                //  search is not on a page called "search.html" at the root (/search.html),
                //  ensure you update the function to reference the correct page.
                function search() {
                    var input = document.getElementById('search-input').value;
                    if (input === '') input = '*';
                    console.log("/search/#/?resource_type=Photos&q=" + input);
                    window.location = "/search/#/?resource_type=Homes&q=" + input;
                    return false;
                }
            </script>
            <form class="form-margin">
                <div class="form-container indent">
                    <label class="hidden" for="name">Search</label>
                    <input type="text" class="text-wide" id="search-input" name="q">
                    <span class="font14" style="display:block">e.g. name of Home, orphanage, organisation or suburb</span>
                </div>
                <div class="form-container indent">
                    <button class="btn" onclick="return search()">Search</button>
                </div>
            </form>

            <div class="or font24"><div class="or2"> OR </div></div>
			<h2>Browse Lists of Homes</h2>
			<table style="width:70%" align="center" cellpadding="2">
				<tr>
					<td width="270"></td>
					<td width="70"></td>
					<td width="100"></td>
					<td width="100"></td>
				<tr>
					<td>Australian Capital Territory</td>
					<td><a href="/ref/act/browse_h_function.htm#F000103">A-Z</a></td>
					<td><a href="/ref/act/browse_by_location.html">By location</a></td>
					<td><a href="/ref/act/browse_1950s.html">By decade</a></td>
				</tr>
				<tr>
					<td>New South Wales</td>
					<td><a href="/ref/nsw/browse_h_function.htm#F000103">A-Z</a></td>
					<td><a href="/ref/nsw/browse_by_location.html">By location</a></td>
					<td><a href="/ref/nsw/browse_1800s.html">By decade</a></td>
				</tr>
				<tr>
					<td>Northern Territory</td>
					<td><a href="/ref/nt/browse_h_function.htm#F000103">A-Z</a></td>
					<td><a href="/ref/nt/browse_by_location.html">By location</a></td>
					<td><a href="/ref/nt/browse_1870s.html">By decade</a></td>
				</tr>
				<tr>
					<td>Queensland</td>
					<td><a href="/ref/qld/browse_h_function.htm#F000103">A-Z</a></td>
					<td><a href="/ref/qld/browse_by_location.html">By location</a></td>
					<td><a href="/ref/qld/browse_1860s.html">By decade</a></td>
				</tr>
				<tr>
					<td>South Australia</td>
					<td><a href="/ref/sa/browse_h_function.htm#F000103">A-Z</a></td>
					<td><a href="/ref/sa/browse_by_location.html">By location</a></td>
					<td><a href="/ref/sa/browse_1840s.html">By decade</a></td>
				</tr>
				<tr>
					<td>Tasmania</td>
					<td><a href="/ref/tas/browse_h_function.htm#F000103">A-Z</a></td>
					<td><a href="/ref/tas/browse_by_location.html">By location</a></td>
					<td><a href="/ref/tas/browse_1820s.html">By decade</a></td>
				</tr>
				<tr>
					<td>Victoria</td>
					<td><a href="/ref/vic/browse_h_function.htm#F000103">A-Z</a></td>
					<td><a href="/ref/vic/browse_by_location.html">By location</a></td>
					<td><a href="/ref/vic/browse_1840s.html">By decade</a></td>
				</tr>
				<tr>
					<td>Western Australia</td>
					<td><a href="/ref/wa/browse_h_function.htm#F000103">A-Z</a></td>
					<td><a href="/ref/wa/browse_by_location.html">By location</a></td>
					<td><a href="/ref/wa/browse_1830s.html">By decade</a></td>
				</tr>
			</table>
        </div>
    </div>
</div>

<?php get_footer(); ?>
