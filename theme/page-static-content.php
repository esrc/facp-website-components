<?php
/**
 * Template Name: Static content, page template.
 * Description:
 * @package FindAndConnect
 */

get_header(); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="aside" id="subnav" role="complementary">
            <?php get_sidebar(); ?>
        </div>
        <div class="section">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            <?php endwhile; else: ?>
            <?php endif; ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>