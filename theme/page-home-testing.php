<?php
/**
 * Template Name: Home page, testing site
 * @package FindAndConnect
 */
get_header(); ?>

<div class="row nav-outer">
    <div class="row container nav" id="nav" role="navigation">
        <ul>
            <li class="nav-home current-home"><a href="/">Home</a></li>
            <li class="nav-about"><a href="/about/"><strong class="nav-large">ABOUT</strong> <span class="nav-small">Find &amp; Connect</span></a></li>
            <li class="nav-homes"><a href="/look-for-homes/"><span class="nav-small">Look for</span> <strong class="nav-large">HOMES</strong></a></li>
            <li class="nav-photos"><a href="/look-for-photos/"><span class="nav-small">Look for</span> <strong class="nav-large">PHOTOS</strong></a></li>
            <li class="nav-records"><a href="/look-for-records/"><span class="nav-small">Look for</span> <strong class="nav-large">RECORDS</strong></a></li>
            <li class="nav-browse"><a href="/browse/"><strong class="nav-large">BROWSE</strong> <span class="nav-small">Find &amp; Connect</span></a></li>
            <li class="nav-help"><a href="/help/"><strong class="nav-large">HELP</strong> <span class="nav-small">with this site</span></a></li>
            <li class="nav-contact"><a href="/contact/"><strong class="nav-large">CONTACT</strong> <span class="nav-small">support/counselling</span></a></li>
        </ul>
    </div>
</div>

<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">
            <h1>Find &amp; Connect web resource testing site</h1>
            <p>
                This is the Find &amp; Connect web resource <b>testing site</b>. Only selected elements and services
                under current review will be present on this site and in sync with the
                <a href="http://www.findandconnect.gov.au">production web site</a>.
            </p>
            <p>
                Please <a href="http://www.findandconnect.gov.au/contact/ask-us/">contact</a> the Find &amp; Connect web
                resource team if you have any questions about the site.
            </p>
        </div>
    </div>
</div>

<?php get_footer(); ?>
