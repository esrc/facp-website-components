<?php
/**
 * Template Name: Browse section, search results, page template.
 * @package FindAndConnect
 */
get_header('nocache'); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="aside" id="subnav" role="complementary">

            <!-- facet selection controller -->
            <div class="dots"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-controller="FacetSelectionController"
                ng-show="items.length>0"
                ng-cloak>
                <p><b>Filtering By</b></p>
                <ul class="items unstyled">
                    <li class="facet" ng-repeat="item in items">
                        <i class="icon-tag"></i>
                        <a ng-click="remove($index)">
                            <span class="value">{{item | prettyFacetLabel | swapFacetLabels | substitute }}</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /facet selection controller -->

            <!-- location facet controller -->
            <div class="dots"
                ng-controller="FieldFacetController"
                ng-hide="selected && exclusive"
                data-exclusive="true"
                data-field="region"
                data-max-items="8"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-cloak>
                <p><b>State</b></p>
                <ul class="facets unstyled">
                    <li ng-repeat="item in items" ng-show="items">
                        <span class="facet-name"><a href="#" ng-click="add($event,$index)">{{item.value | substitute }}</a></span>
                        <span class="facet-score" ng-show="item.score">({{item.score}})</span>
                    </li>
                </ul>
            </div>
            <!-- /location facet controller -->

            <!-- function facet controller -->
            <div class="dots"
                ng-controller="FieldFacetController"
                ng-hide="selected && exclusive"
                data-exclusive="false"
                data-field="function"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-cloak>
                <p><b>Function</b></p>
                <ul class="facets unstyled">
                    <li ng-repeat="item in items" ng-show="items">
                        <span class="facet-name"><a href="#" ng-click="add($event,$index)">{{item.value}}</a></span>
                        <span class="facet-score" ng-show="item.score">({{item.score}})</span>
                    </li>
                </ul>
            </div>
            <!-- function facet controller -->

            <!-- type facet controller -->
            <div class="dots"
                ng-controller="FieldFacetController"
                ng-hide="selected && exclusive"
                data-exclusive="true"
                data-field="localtype"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-cloak>
                <p><b>Type</b></p>
                <ul class="facets unstyled">
                    <li ng-repeat="item in items" ng-show="items">
                        <span class="facet-name"><a href="#" ng-click="add($event,$index)">{{item.value}}</a></span>
                        <span class="facet-score" ng-show="item.score">({{item.score}})</span>
                    </li>
                </ul>
            </div>
            <!-- /type facet controller -->

            <!-- date range facet controller -->
            <div id="date-range-panel"
                class="facet-panel"
                ng-controller="DateFacetController"
                data-start-date-field="fromDate"
                data-end-date-field="toDate"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-cloak>
                <p><b>Dates</b></p>
                <form class="form-horizontal" ng-submit="submit()" style="margin-bottom:0px;">
                    <div id="date-range-histogram"></div>
                    <table class="table-horizontal">
                        <tr>
                            <td style="font-size:small">
                                <div class="input-prepend">
                                    <span class="add-on">From</span>
                                    <input id="fromDate" name="startDate" class="year-input" type="text" ng-model="startDate" size="4" />
                                </div>
                            </td>
                            <td class="pull-right" style="font-size:small;align:right">
                                <div class="input-prepend pull-right">
                                    <span class="add-on">To</span>
                                    <input id="toDate" name="endDate" class="year-input" type="text" ng-model="endDate" size="4" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <button style="position: absolute; left: -9999px; width: 1px; height: 1px;">Update</button>
                </form>
            </div>
            <!-- /date range facet controller -->

        </div>
        <div class="section">

            <div class="notice">
                Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
            </div>

            <form class="search-box"
                  ng-controller="SimpleSearchBoxController"
                  ng-submit="handleSubmit()"
                  data-near-match="true"
                  data-source="https://data.esrc.unimelb.edu.au/solr/FACP">
                <input class="text-wide" name="q" type="text" placeholder="Keyword or place name" ng-model="userQuery" />
                &nbsp;
                <button class="btn" name="submit" type="submit" ng-click="handleSubmit()">Search</button>
            </form>

            <div class="search-results"
                 ng-controller="DocumentSearchResultsController"
                 data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                 ng-cloak>

                <div class="summary ng-binding">
                    You searched for '{{userquery}}'. The query returned {{totalResults}} documents.
                    <span ng-show="documents.length > 0">Showing page {{page + 1}} of
                    {{totalPages}} pages.</span>
                </div>

                <div class="document" ng-repeat="doc in documents">
                    <div class="title"><a ng-href="{{doc.presentation_url}}">{{doc | categoryLabel}} - {{doc.title | truncate:80 }}</a></div>
                    <div class="thumb" style="margin-left:10px;" ng-show="doc.dobj_proxy_small">
                        <a ng-href="{{doc.presentation_url}}" class="thumbnail">
                            <img ng-src="{{doc.dobj_proxy_small}}" alt="{{doc.title}}" height="64px" width="64px" />
                        </a>
                    </div>
                    <div class="meta inline">
                        <div class="existdates" ng-show="doc.fromDate">
                            {{doc.fromDate | strip | trim}} &ndash; {{doc.toDate | strip | trim}};
                        </div>
                        <div class="region" ng-show="doc.region">{{doc.region | substitute }}</div>
                    </div>
                    <div class="abstract">{{doc.abstract | truncate:250 }}</div>
                    <div class="url"><a ng-href="{{doc.presentation_url}}">{{doc.presentation_url}}</a></div>
                </div>
                <div class="pager" ng-hide="error">
                    <ul>
                        <li ng-repeat="page in pages">
                            <a ng-class="{iscurrent:page.isCurrent}" ng-click="$parent.handleSetPage(page.number)">{{page.name}}</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- scripts -->
<script src="/legacy-search/jquery-ui/jquery.min.js"></script>
<script src="/legacy-search/jquery-ui/jquery-ui.js"></script>
<script src="/legacy-search/angular/angular.min.js"></script>

<script src="/legacy-search/controllers/DateFacetController.js"></script>
<script src="/legacy-search/controllers/DocumentSearchResultsController.js"></script>
<script src="/legacy-search/controllers/FacetSelectionController.js"></script>
<script src="/legacy-search/controllers/FieldFacetController.js"></script>
<script src="/legacy-search/controllers/SimpleSearchBoxController.js"></script>
<script src="/legacy-search/directives/autocomplete.js"></script>
<script src="/legacy-search/filters/textfilters.js"></script>
<script src="/legacy-search/services/solr.js"></script>
<script src="/legacy-search/services/utils.js"></script>
<script src="/legacy-search/app-browse.js"></script>

<?php get_footer(); ?>
