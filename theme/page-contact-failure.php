<?php
/**
 * Template Name: Contact By Email failure, page template
 * @package FindAndConnect
 */

get_header(); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">
 
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            <?php endwhile; else: ?>
            <?php endif; ?>

        </div>
    </div>
</div>

<?php get_footer(); ?>