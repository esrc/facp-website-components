<?php
/**
 * Page footer.
 * @package FindAndConnect
 */
?>

<script type="text/javascript">
    function send(url) {
        window.location = url + "?u=" + encodeURIComponent(window.location) + "&t=" + encodeURIComponent(document.title);
    }
</script>
<div class="row footer-rich-outer">
	<div class="row container footer-rich">
		<div class="col">
 <h4 class="mbs">About</h4>
			<ul class="list-bullet">
				<li><a href="/about/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'About']);">About Find & Connect</a></li>
				<li><a href="/about/acknowledgement/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Acknowledgement']);">Acknowledgement</a></li>
				<li><a href="/about/background/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Background']);">Background</a></li>
				<li><a href="/about/accessibility/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Accessibility']);">Accessibility</a></li>
				<li><a href="/about/web-resource-credits/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Web Resource Credits']);">Credits</a></li>
				<li><a href="/about/feedback/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Feedback']);">Feedback</a></li>
				<li><a href="/about/content-warning/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Content Warning']);">Content Warning</a></li>
				<li><a href="/about/terms-and-conditions/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Terms and Conditions']);">Terms & Conditions</a></li>
				<li><a href="/help/factsheet-1-how-to-use-the-website/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'How to use this Site']);">How to use this Site</a></li>
				<li><a href="/help/faqs/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'FAQs']);">Frequently Asked Questions (FAQs)</a></li>
			</ul>
		</div>
		<div class="col">
			<h4 class="mbs">Information about Records</h4>
			<ul class="list-bullet">
				<li><a href="/resources/what-to-expect-when-accessing-records/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'What to Expect']);">What to Expect when Accessing Records about You</a></li>
				<li><a href="/resources/where-to-start/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Where to Start']);">Where to Start</a></li>
				<li><a href="/resources/your-rights/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Applying for Records']);">Applying for Records: Your Rights and the Law</a></li>
				<li><a href="/resources/records-from-salvation-army-homes/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Salvation Army Homes']);">Salvation Army Homes</a></li>
				<li><a href="/resources/disability-homes/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Disability Homes']);">Disability Homes</a></li>
				<li><a href="/resources/former-child-migrants/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Former Child Migrants']);">Former Child Migrants</a></li>
				<li><a href="/resources/adoptions/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Adoptions']);">Adoptions</a></li>
				<li><a href="/resources/family-tracing/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Family Tracing']);">Family Tracing</a></li>
				<li><a href="/resources/family-history/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Family History']);">Family History</a></li>
				<li><a href="/resources/searching-for-records-of-a-parent-or-grandparent/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Searching for Records']);">Searching for Records of a Parent or Grandparent</a></li>
			</ul>     
		</div>
		<div class="col">
		<h4 class="mbs">Featured Stories</h4>
			<ul class="list-bullet">
			<li><a href="/featured-stories/timeline/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Timeline of Major Events']);">Timeline of Events & Legislation</a></li>
			</ul>
			<br/>
			<h4 class="mbs">Resources</h4>
			<ul class="list-bullet">
				<li><a href="/resources/radp/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Resources for Record Holders']);">Resources for Record Holders</a></li>
			</ul>
			</div>
		<div class="col col-last">
     <h4 class="mbs">Connect</h4>			
		<ul class="list-social">
			<li class="list-social-twitter"><a href="https://twitter.com/FaCWebResource" target="_blank" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Follow on Twitter']);">Twitter</a></li>
			<li class="list-social-youtube"><a href="https://www.youtube.com/user/FindandConnect/" target="_blank" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'YouTube']);">YouTube</a></li>
			<li class="list-social-blog"><a href="http://www.findandconnectwrblog.info/" target="_blank" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'News']);">Blog</a></li>
			<li class="list-social-support-services"><a href="/contact/" onClick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Contact']);">Support Services</a></li>
		</ul>
		</div>
	</div>
</div>
<div class="row footer-outer">
	<div class="row container footer" role="contentinfo">
		<div class="section-home">
			<p>
				<img src="/assets/img/footer-logo.png" alt="Find &amp; Connect">
			</p>
			<p class="font13">
				Cite this: <?php echo current_page_url(); ?><br>
				First published by the Find &amp; Connect Web Resource Project for the Commonwealth of Australia, 2011
			</p>
		</div>
		<div class="aside-home">
			<p class="cc-logo">
				<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/au/"><img src="/assets/img/by-nc-sa.png" alt="Creative commons"></a>
			</p>
			<p class="font13 cc-text">
				Except where otherwise noted, content on this site is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/au/">Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License</a>
			</p>
		</div>
	</div>
</div>
<script>
	var navigation = responsiveNav("#nav", {label: "Site menu"});
	var subnav = responsiveNav("#subnav", {label: "Section menu"});
</script> 
</body>
</html>

