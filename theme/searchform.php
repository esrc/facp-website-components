<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

<div class="entry">
<h3>Search News</h3>
<form id="news-searchform" action="<?php bloginfo('url'); ?>" method="get">
  <div>
    <label class="searchlabel" for="s">Search</label>
    <input type="text" name="s" id="s" size="15" value="<?php the_search_query(); ?>" />
    <input type="hidden" value="post" name="post_type" id="post_type" />
    <input id="go" name="go" alt="Search" type="submit" value="GO" />
  </div>
</form>
</div>