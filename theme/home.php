<?php
/**
 * Description: News Home template.
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header('news'); ?>

<div class="row main-outer">
	<div class="row container main">
<div class="section">
		<?php if (have_posts()) : ?>
		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts("posts_per_page=2&paged=$paged"); ?>
        <?php while (have_posts()) : the_post(); ?>
		<div class="entry">
			<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
			<p class="metadetails">
			 Posted on <?php the_time('j F Y') ?> by <?php the_author_posts_link(); ?><br />
			 <?php the_tags(); ?>		
			</p>
				<?php the_excerpt(); ?>
			 </div>
	        <?php endwhile; else: ?>
		      <p>Sorry, no posts matched your criteria.</p>
	        <?php endif; ?>						
		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('Older posts') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer posts') ?></div>
		</div>
	</div>	
<?php get_sidebar('news'); ?>
</div>
</div>	
<?php get_footer('news'); ?>

