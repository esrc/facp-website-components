<?php
/**
 * Template Name: Look for Records, search results page template
 * @package FindAndConnect
 */
get_header('nocache'); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="aside" role="complementary">
            <div class="dots">
                <a class="question" href='#' onclick="send('/contact/support-service/')">Send message to <strong>Find &amp; Connect support service</strong></a>
            </div>
        </div>
        <div class="section">

            <div class="notice">
                Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
            </div>

            <h1>Look for records: search results</h1>

            <form class="search-box"
                  ng-controller="SearchBoxController"
                  data-near-match="true"
                  data-source="https://data.esrc.unimelb.edu.au/solr/FACP">
                <input class="text-wide" name="q" type="text" placeholder="Keyword or placename" ng-model="userquery" searchbox />
                &nbsp;
                <button class="btn" name="submit" type="submit" ng-click="handleSubmit()" searchbutton>Search</button>
            </form>

            <div class="search-results"
                 ng-controller="DocumentSearchResultsController"
                 data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                 ng-cloak>

                <div class="summary ng-binding">
                    You searched for '{{userquery}}'. The query returned {{totalResults}} documents.
                    <span ng-show="documents.length > 0">Showing page {{page + 1}} of
                    {{totalPages}} pages.</span>
                </div>

                <div class="document" ng-repeat="doc in documents">
                    <div class="title"><a ng-href="{{doc.presentation_url}}">{{doc.title | truncate:80}}</a></div>
                    <div class="meta inline">
                        <div class="existdates" ng-show="doc.fromDate">
                            {{doc.fromDate | strip | trim}} &ndash; {{doc.toDate | strip | trim}};
                        </div>
                        <div class="region" ng-show="doc.region">{{doc.region | substitute}}</div>
                    </div>
                    <div class="abstract">{{doc.abstract | truncate:250 }}</div>
                    <div class="url"><a ng-href="{{doc.presentation_url}}">{{doc.presentation_url}}</a></div>
                </div>
                <div class="pager" ng-hide="error">
                    <ul>
                        <li ng-repeat="page in pages">
                            <a ng-class="{iscurrent:page.isCurrent}" ng-click="$parent.handleSetPage(page.number)">{{page.name}}</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- scripts -->
<script src="/legacy-search/jquery-ui/jquery.min.js"></script>
<script src="/legacy-search/jquery-ui/jquery-ui.js"></script>
<script src="/legacy-search/angular/angular.min.js"></script>

<script src="/legacy-search/controllers/DocumentSearchResultsController.js"></script>
<script src="/legacy-search/controllers/SearchBoxController.js"></script>
<script src="/legacy-search/directives/autocomplete.js"></script>
<script src="/legacy-search/filters/textfilters.js"></script>
<script src="/legacy-search/services/solr.js"></script>
<script src="/legacy-search/services/utils.js"></script>
<script src="/legacy-search/app.js"></script>

<?php get_footer(); ?>