<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>

<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8" lang="en-US"><![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!--><html lang="en-US"><!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="Author" lang="en" content="Find and Connect Web Resource Project, The University of Melbourne and Australian Catholic University" />
  <meta name="Description" lang="en" content="Find and Connect News - Find and Connect is a resource for people who as children were in out-of-home 'care' in Australia. It contains information about organisations, people, policies, legislation and events related to the history of child welfare." />
	<meta name="Keywords" lang="en" content="Find and Connect News, Find and Connect Australia, Find and Connect, Forgotten Australians, Former Child Migrants, Stolen Generations, history of child welfare in Australia" />
  <meta name="DC.Title" lang="en" content="Find and Connect News" />
	<meta name="DC.Creator" lang="en" content="Find and Connect Web Resource Project, The University of Melbourne and Australian Catholic University" />
	<meta name="DC.Subject" lang="en" content="Find and Connect News, Find and Connect Australia, Find and Connect, Forgotten Australians, Former Child Migrants, Stolen Generations, history of child welfare in Australia" />
	<meta name="DC.Description" lang="en" content="Find and Connect News - Find and Connect is a resource for people who as children were in out-of-home 'care' in Australia. It contains information about organisations, people, policies, legislation and events related to the history of child welfare." />
	<meta name="DC.Publisher" lang="en" content="eScholarship Research Centre, The University of Melbourne" />
	<meta name="DC.Date.Created" scheme="ISO8601" lang="en" content="2010-08-10" />
	<meta name="DC.Date.LastModified" scheme="ISO8601" lang="en" content="2014-06-16" />
	<meta name="DC.Type" lang="en" content="Document" />
	<meta name="DC.Format" scheme="IMT" lang="en" content="text/html" />
  <meta name="DC.Identifier" scheme="URL" lang="en" content="<?php echo get_option('home'); ?>/" />
  <meta name="DC.Language" scheme="ISO639" lang="en" content="en-gb" />
	<meta name="DC.Rights" lang="en" content="Find and Connect Web Resource Project, The University of Melbourne and Australian Catholic University" />
	<meta name="robots" content="index, follow" />
	<meta name="robots" content="noodp" />
	<meta name="robots" content="noydir" />
	<link rel="stylesheet" href="/assets/news/normalize.css" />
	<link rel="stylesheet" href="/assets/news/base.css" />
	<link rel="stylesheet" href="/assets/news/layout.css" />
	<link rel="stylesheet" href="/assets/news/news.css" />
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-26872007-1']);
	_gaq.push(['_trackPageview']);
	(function() {
	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
	</script>
</head>

<body>
<div class="row header-outer">
	<div class="row container header" role="banner">
		<div class="header-logo">
			<a href="/news"><img src="/assets/news/images/header-logo-news.png" alt="Find & Connect News"></a> 
</div>
		<div class="header-details">
			<span class="header-details-small">Find &amp; Connect Support Services</span><br>
			<span class="header-details-large"><strong><a class="tel" href="tel:+611800161109">1800 16 11 09</a></strong></span><br>
			<span class="header-details-small">Freecall: Monday-Friday 9am-5pm</span> 
		</div>
	</div>
</div>
<div class="row nav-outer">
	<div class="row container nav">
	</div>
</div>	