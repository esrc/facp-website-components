<?php
/**
 * Template Name: Newsletters listing, page template
 * @package FindAndConnect
 */
get_header('news'); ?>

<div class="row main-outer">
	<div class="row container main">
<div class="section">
            <h1>Newsletters</h1>
            <?php
              $query = new WP_Query(array('post_type'=>array('newsletters'),
                                          'status'=>array('Public'),
                                          'posts_per_page' => -1,
                                          'paged' => $paged));
              while ($query->have_posts()) : $query->the_post();
            ?>
            <div class="post">
                <h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                <small><?php the_time('F jS, Y'); ?> by <?php the_author_posts_link(); ?></small>
                <div class="entry"><?php the_excerpt(); ?></div>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
        </div>
<?php get_sidebar('news'); ?>
</div>
</div>	
<?php get_footer('news'); ?>