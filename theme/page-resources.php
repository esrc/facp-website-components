<?php
/**
 * Template Name: Resources section, page template.
 * @package FindAndConnect
 */

get_header(); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">
            <div class="notice">
                Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
            </div>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            <?php endwhile; else: ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>