<?php
/**
 * Template Name: Contact By Email form, page template
 * @package FindAndConnect
 */
get_header(); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">

            <h1>Send feedback about website</h1>
            <p>
                This form will send an email to the Find & Connect web team. Read our
                <a href="/about/terms-and-conditions/#privacy">privacy policy</a>.
            </p>

            <form class="form-margin" action="http://mailer.esrc.unimelb.edu.au/send/FACP" method="post">

                <input type="hidden" id="team" name="team" value="web_resource" readonly>
                <input type="hidden" id="referring_page_title" name="referring_page_title" value="" readonly />
                <input type="hidden" id="referring_page_url" name="referring_page_url" value="" readonly />
                <input type="hidden" id="formtype" name="formtype" value="askus" />

                <div id="js_referrer"></div>

				<div class="form-container">
					<label class="label-wide" for="feedback"><strong>Your message</strong></label>
					<textarea class="textarea-wide" name="feedback" rows="5" cols="50" required></textarea>
				</div>
				<div class="form-container">
					<label class="label-wide" for="name"><strong>Your name</strong></label>
					<input class="text-wide" name="name" id="name" type="text" required />
					<div class="indent font14">We need to know this so that we know how to address you</div>
				</div>
				<div class="form-container">
					<label class="label-wide" for="email"><strong>Your email</strong></label>
					<input class="text-wide" name="email" id="email" type="email" required />
					<div class="indent font14">We need to know this so that we can respond to your feedback</div>
				</div>
				<div class="indent">
					<input class="btn" name="submit" id="submit" type="submit" value="&nbsp;&nbsp;Send&nbsp;&nbsp;">
				</div>

				<!-- extract arguments from the query and assign them to the
				     hidden form fields
				-->
                <script src="https://www.esrc.unimelb.edu.au/lib/contact.js"></script>
			</form>

        </div>
    </div>
</div>

<?php get_footer(); ?>
