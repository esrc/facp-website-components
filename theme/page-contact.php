<?php
/**
 * Template Name: Contact section, page template.
 * @package FindAndConnect
 */
get_header(); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="aside" id="subnav" role="complementary">

            <div class="dots">
                <a class="question" href='#' onclick="send('/contact/support-service/')">Send message to <strong>Find &amp; Connect support service</strong></a>
            </div>
<div class="dots">
        <ul class="list-bullet">
         <?php dynamic_sidebar( 'Contact Sidebar' ); ?>
       </ul>
</div>

</div>
        <div class="section">
            <div class="notice">
                Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
            </div>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            <?php endwhile; else: ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>