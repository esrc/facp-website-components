<?php
/**
 * Template Name: Look for Photos, search results page template
 * @package FindAndConnect
 */
get_header('nocache'); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="aside" role="complementary">
            <div class="dots">
                <a class="question" href='#' onclick="send('/contact/support-service/')">Send message to <strong>Find &amp; Connect support service</strong></a>
            </div>
        </div>
        <div class="section">

            <div class="notice">
                Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
            </div>

            <h1>Look for photos: search results</h1>

            <form class="search-box"
                  ng-controller="SearchBoxController"
                  data-near-match="true"
                  data-source="https://data.esrc.unimelb.edu.au/solr/FACP">
                <input class="text-wide" name="q" type="text" placeholder="Keyword or placename" ng-model="userquery" searchbox />
                &nbsp;
                <button class="btn" name="submit" type="submit" ng-click="handleSubmit()" searchbutton>Search</button>
            </form>

            <div class="search-results"
                 ng-controller="ImageSearchResultsController"
                 data-documents-per-row="4"
                 data-documents-per-set="12"
                 data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                 ng-cloak>

                <div class="summary ng-binding">
                    You searched for '{{userquery}}'. The query returned {{totalResults}} documents.
                    <span ng-show="totalResults > 0">Showing page {{page + 1}} of
                    {{totalPages}} pages.</span>
                </div>

                <div ng-repeat="row in rows" class="image-row">
                    <div ng-repeat="image in row" class="image">
                        <div class="thumb">
                            <a ng-href="{{image.presentation_url}}">
                                <img ng-src="{{image.dobj_proxy_medium}}" alt="{{image.title}}" />
                            </a>
                        </div>
                        <div class="caption">
                            <a ng-href="{{image.presentation_url}}">{{image.title | truncate:80 }}</a>
                        </div>
                    </div>
                </div>

                <div class="pager" ng-hide="error">
                    <ul>
                        <li ng-repeat="page in pages">
                            <a ng-class="{iscurrent:page.isCurrent}" ng-click="$parent.handleSetPage(page.number)">{{page.name}}</a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- scripts -->
<script src="/legacy-search/jquery-ui/jquery.min.js"></script>
<script src="/legacy-search/jquery-ui/jquery-ui.js"></script>
<script src="/legacy-search/angular/angular.min.js"></script>

<script src="/legacy-search/controllers/ImageSearchResultsController.js"></script>
<script src="/legacy-search/controllers/SearchBoxController.js"></script>
<script src="/legacy-search/directives/autocomplete.js"></script>
<script src="/legacy-search/filters/textfilters.js"></script>
<script src="/legacy-search/services/solr.js"></script>
<script src="/legacy-search/services/utils.js"></script>
<script src="/legacy-search/app.js"></script>

<?php get_footer(); ?>