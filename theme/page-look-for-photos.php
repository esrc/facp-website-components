<?php
/**
 * Template Name: Look for Photos page template
 * @package FindAndConnect
 */
get_header('nocache'); ?>

<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">

            <div class="notice">
                Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
            </div>

            <h1>
                Look for Photos
            </h1>
			
            <h2>
                Quick Search
            </h2>
               
            <p>
				NOTE: Searching this website may not work with some versions of the Internet Explorer browser. For more information on the issue please read our <a href="http://www.findandconnectwrblog.info/2015/07/issue-with-search/">blog post</a>.
            </p>
            
            <script type="text/javascript">
                // This function is required on all pages that have a search box. The
                //  submit button MUST be connected to this function. Also, ensure
                //  the id of the input element is "search-form". If your faceted
                //  search is not on a page called "search.html" at the root (/search.html),
                //  ensure you update the function to reference the correct page.
                function search() {
                    var input = document.getElementById('search-input').value;
                    if (input === '') input = '*';
                    console.log("http://www.findandconnect.gov.au/search/#/?resource_type=Photos&q=" + input);
                    window.location = "http://www.findandconnect.gov.au/search/#/?resource_type=Photos&q=" + input;
                    return false;
                }
            </script>
            <form class="form-margin">
                <div class="form-container indent">
                    <label class="hidden" for="name">Search</label>
                    <input type="text" class="text-wide" id="search-input" name="q">
                    <span class="font14" style="display:block">e.g. name of Home, orphanage, organisation or suburb</span>
                </div>
                <div class="form-container indent">
                    <button class="btn" onclick="return search()">Search</button>
                </div>
            </form>

        </div>
    </div>
</div>

<?php get_footer(); ?>