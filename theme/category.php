<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header('news'); ?>

<div class="row main-outer">
	<div class="row container main">
<div class="section">
<h2>Category: <?php single_cat_title(); ?></h2>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="entry">
			<h3><a href="<?php the_permalink() ?>" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			<p class="metadetails">
			 Posted on <?php the_time('j F Y') ?> by <?php the_author_posts_link(); ?><br />
			 <?php the_tags(); ?>			
			</p>
			<?php the_excerpt(); ?>
			 </div>
	        <?php endwhile; else: ?>
		      <p>Sorry, no posts matched your criteria.</p>
	        <?php endif; ?>						
		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('Older entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer entries') ?></div>
		</div>
	</div>	
<?php get_sidebar('news'); ?>
</div>
</div>	
<?php get_footer('news'); ?>


