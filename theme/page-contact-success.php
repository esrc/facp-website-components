<?php
/**
 * Template Name: Contact By Email success, page template
 * Description:
 * @package FindAndConnect
 */

get_header(); ?>

<!-- page-contact-success.php -->
<div class="row main-outer">
    <div class="row container main">
        <div class="section-wide">

            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            <?php endwhile; else: ?>
            <?php endif; ?>

            <a id="redirect_link" href="/">Return to web site</a>

            <!-- this script will extract a redirect title and URL value from
                 the browser location and then update the "return to web site"
                 link with the specified values
            -->
            <script type="text/javascript">
                // Get the redirect value from the query portion of the current
                // browser location
                var components = window.location.search.split('&');
                if (components.length > 1) {
                    var url = components.shift();
                    if (components.length > 1) {
                        var title = components.join('&');
                    } else {
                        var title = components.join('');
                    }
                    if (url.substr(0,3) === '?u=' && title.substr(0,2) === 't=') {
                        url = decodeURIComponent(url.slice(3));
                        title = decodeURIComponent(title.slice(2));

                        if (url !== undefined) {
                            var a = document.getElementById('redirect_link');
                            if (title !== undefined) {
                                a.href = url;
                                a.innerText = "Return to '" + title + "'";
                            }
                        }
                    }
                }
            </script>

        </div>
    </div>
</div>

<?php get_footer(); ?>