<?php
/**
 * Template Name: Browse section, map search, page template.
 * @package FindAndConnect
 */

get_header(); ?>

<!-- page-browse-map-search.php -->
<div class="row nav-outer">
    <div class="row container nav" id="nav" role="navigation">
        <ul>
            <li class="nav-home"><a href="/">Home</a></li>
            <li class="nav-about"><a href="/about/"><strong class="nav-large">ABOUT</strong> <span class="nav-small">Find &amp; Connect</span></a></li>
            <li class="nav-homes"><a href="/look-for-homes/"><span class="nav-small">Look for</span> <strong class="nav-large">HOMES</strong></a></li>
            <li class="nav-photos"><a href="/look-for-photos/"><span class="nav-small">Look for</span> <strong class="nav-large">PHOTOS</strong></a></li>
            <li class="nav-records"><a href="/look-for-records/"><span class="nav-small">Look for</span> <strong class="nav-large">RECORDS</strong></a></li>
            <li class="nav-browse current"><a href="/browse/"><strong class="nav-large">BROWSE</strong> <span class="nav-small">Find &amp; Connect</span></a></li>
            <li class="nav-help"><a href="/help/"><strong class="nav-large">HELP</strong> <span class="nav-small">with this site</span></a></li>
            <li class="nav-contact"><a href="/contact/"><strong class="nav-large">CONTACT</strong> <span class="nav-small">support/counselling</span></a></li>
        </ul>
    </div>
</div>

<div class="row main-outer">
    <div class="row container main">
        <div class="aside" id="subnav" role="complementary">

            <!-- facet selection controller -->
            <div class="dots"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-controller="FacetSelectionController"
                ng-show="items.length>0"
                ng-cloak>
                <p><b>Filtering By</b></p>
                <ul class="items unstyled">
                    <li class="facet" ng-repeat="item in items">
                        <i class="icon-tag"></i>
                        <a ng-click="remove($index)">
                            <span class="value">{{item | prettyFacetLabel | swapFacetLabels | substitute }}</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- /facet selection controller -->

            <!-- location facet controller -->
            <div class="dots"
                ng-controller="FieldFacetController"
                ng-hide="selected && exclusive"
                data-exclusive="true"
                data-field="region"
                data-max-items="8"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-cloak>
                <p><b>State</b></p>
                <ul class="facets unstyled">
                    <li ng-repeat="item in items" ng-show="items">
                        <span class="facet-name"><a href="#" ng-click="add($event,$index)">{{item.value | substitute }}</a></span>
                        <span class="facet-score" ng-show="item.score">({{item.score}})</span>
                    </li>
                </ul>
            </div>
            <!-- /location facet controller -->

            <!-- function facet controller -->
            <div class="dots"
                ng-controller="FieldFacetController"
                ng-hide="selected && exclusive"
                data-exclusive="false"
                data-field="function"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-cloak>
                <p><b>Function</b></p>
                <ul class="facets unstyled">
                    <li ng-repeat="item in items" ng-show="items">
                        <span class="facet-name"><a href="#" ng-click="add($event,$index)">{{item.value}}</a></span>
                        <span class="facet-score" ng-show="item.score">({{item.score}})</span>
                    </li>
                </ul>
            </div>
            <!-- function facet controller -->

            <!-- type facet controller -->
            <div class="dots"
                ng-controller="FieldFacetController"
                ng-hide="selected && exclusive"
                data-exclusive="false"
                data-field="localtype"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-cloak>
                <p><b>Type</b></p>
                <ul class="facets unstyled">
                    <li ng-repeat="item in items" ng-show="items">
                        <span class="facet-name"><a href="#" ng-click="add($event,$index)">{{item.value}}</a></span>
                        <span class="facet-score" ng-show="item.score">({{item.score}})</span>
                    </li>
                </ul>
            </div>
            <!-- /type facet controller -->

            <!-- date range facet controller -->
            <div id="date-range-panel"
                class="facet-panel"
                ng-controller="DateFacetController"
                data-start-date-field="fromDate"
                data-end-date-field="toDate"
                data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                ng-cloak>
                <p><b>Dates</b></p>
                <form class="form-horizontal" ng-submit="submit()" style="margin-bottom:0px;">
                    <div id="date-range-histogram"></div>
                    <table class="table-horizontal">
                        <tr>
                            <td style="font-size:small">
                                <div class="input-prepend">
                                    <span class="add-on">From</span>
                                    <input id="fromDate" name="startDate" class="year-input" type="text" ng-model="startDate" size="4" />
                                </div>
                            </td>
                            <td class="pull-right" style="font-size:small;align:right">
                                <div class="input-prepend pull-right">
                                    <span class="add-on">To</span>
                                    <input id="toDate" name="endDate" class="year-input" type="text" ng-model="endDate" size="4" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <button style="position: absolute; left: -9999px; width: 1px; height: 1px;">Update</button>
                </form>
            </div>
            <!-- /date range facet controller -->

        </div>
        <div class="section">
            <div class="notice">
                Some people may find content on this website distressing. <a href="/about/content-warning/">Read more</a>
            </div>

            <form class="search-box"
                  ng-controller="SearchBoxController"
                  ng-submit="handleSubmit()"
                  data-near-match="true"
                  data-source="https://data.esrc.unimelb.edu.au/solr/FACP">
                <input class="text-wide" name="q" type="text" placeholder="Keyword or placename" ng-model="userquery" searchbox />
                &nbsp;
                <button class="btn" name="submit" type="submit" ng-click="handleSubmit()" searchbutton>Search</button>
            </form>

            <div id="map"
                 ng-controller="MapController"
                 data-source="https://data.esrc.unimelb.edu.au/solr/FACP"
                 data-start-latitude='-29.3456'
                 data-start-longitude='141.4346'
                 style="height:500px;width:100%;"></div>

        </div>
    </div>
</div>

<!-- scripts -->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?&sensor=true"></script>
<script type="text/javascript" src="/legacy-search/google/markerclusterer_compiled.js"></script>

<script src="/legacy-search/jquery-ui/jquery.min.js"></script>
<script src="/legacy-search/jquery-ui/jquery-ui.js"></script>
<script src="/legacy-search/angular/angular.min.js"></script>

<script src="/legacy-search/controllers/DateFacetController.js"></script>
<script src="/legacy-search/controllers/FacetSelectionController.js"></script>
<script src="/legacy-search/controllers/FieldFacetController.js"></script>
<script src="/legacy-search/controllers/MapController.js"></script>
<script src="/legacy-search/controllers/SearchBoxController.js"></script>
<script src="/legacy-search/directives/autocomplete.js"></script>
<script src="/legacy-search/filters/textfilters.js"></script>
<script src="/legacy-search/services/selection.js"></script>
<script src="/legacy-search/services/solr.js"></script>
<script src="/legacy-search/services/utils.js"></script>
<script src="/legacy-search/app-map.js"></script>

<?php get_footer(); ?>
