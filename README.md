Find and Connect Public Web Site Assets
=======================================

Assets for Find and Connect web resource, version two.
The project folder structure is as follows:

* /theme                 - Wordpress theme
* /assets                - CSS, Javascript, images and other static content
* /assets/templates/ohrm - OHRM template files
* /assets/templates/app  - FACP application template files
* /search                - Search specific Javascript files
* /scripts               - Linux shell scripts used to update project dependencies and/or prepare parts of the project for deployment


